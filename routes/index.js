'use strict';

var express = require('express'),
	indexController = require('../controllers/indexController'),
	router = express.Router();

router.get('/', function (req, res) {
	indexController.render(req, res);
});

module.exports = router;