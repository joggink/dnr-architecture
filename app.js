'use strict';

var express = require('express'),
	app = express(),
	routes = require('./routes/index'),
	fs = require('fs'),
	glob = require('glob'),
	hbs = require('hbs'),
	hbsLayouts = require('handlebars-layouts');

// 	set our view engine to handlebars
	app.set('view engine', 'hbs');
//	static files
	app.use('/svg', express.static('svg'));
//	static files
	app.use('/public', express.static('public'));
//	using our routes
	app.use('/', routes);

// register partials
hbs.registerHelper(hbsLayouts(hbs.handlebars));

glob("views/**/*.partial.hbs", {}, function (er, files) {
	files.forEach(function(file){
		var arr = file.split('/'),
			partialArr = [],
			template;

		arr.forEach(function(part){
			if(part !== 'views' && part !== 'includes'){
				partialArr.push(part.replace('.partial.hbs', ''));
			}
		});

		template = fs.readFileSync(file, 'utf8');
		hbs.registerPartial(partialArr.join('-'), template);
	});
});

// start our server
var server = app.listen(3000, function() {
	var host = server.address().address;
	var port = server.address().port;
	console.log('Server is running at http://%s/%s', host, port);
});