'use strict';

var data = require('../data/jurgen.json');

module.exports = (function() {
	
	// set default renderview if not passed
	var _renderView = 0,
		_cache = {
			'elements': _getElements(),
			'relations': _getRelations(),
		},
		returnModel = {
		'title': data.name,
		'description': data.description,
		'view': {
			elements: _populateElements()
		}
	};

	function _getElements() {
		var elements = [];
		
		for (var types in data.model) {
			for (var ents in data.model[types]) {
				elements[data.model[types][ents].id] = data.model[types][ents];
			}
		}
		return elements;
	}

	function _getRelations() {
		var relations = [];
		
		for (var types in data.model) {
			for (var ents in data.model[types]) {
				if (data.model[types][ents].relationships && data.model[types][ents].relationships.length) {
					for (var relation in data.model[types][ents].relationships) {
						relations[data.model[types][ents].relationships[relation].id] = data.model[types][ents].relationships[relation];
					}
				}
			}
		}
		return relations;
	}
	
	function _populateElements() {
		var elements = [];
		if (data.views.systemContextViews[_renderView].elements) {
			for (var element in data.views.systemContextViews[_renderView].elements) {
				elements.push(
					_cache.elements[data.views.systemContextViews[_renderView].elements[element].id]
				)
			}
		}
		elements = _enrichRelations(elements);
		return elements;
	}

	function _enrichRelations(elements) {
		for (var element in elements) {
			if (elements[element].relationships && elements[element].relationships.length) {
				for (var relation in elements[element].relationships) {
					elements[element].relationships[relation].sourceElement = _cache.elements[elements[element].relationships[relation].sourceId];
					elements[element].relationships[relation].destinationElement = _cache.elements[elements[element].relationships[relation].destinationId];
				}
			}
		}
		return elements;
	}
	
	return returnModel;
	
}());
