'use strict';

var indexModel = require('../models/indexModel.js');

module.exports = (function() {

	function _render(req, res) {
		res.render('index', indexModel);
	}
	
	return {
		render: _render
	}
	
}());
